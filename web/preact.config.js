module.exports = function (config) {
  if (process.env.NODE_ENV === 'development') {
    if (config && config.devServer) {
      config.devServer.proxy = [
        {
          path: '/api',
          target: 'http://localhost:3000'
        }
      ]
    }
  }
}
