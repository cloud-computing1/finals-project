import axios from 'axios'
import {
  Authorize,
  BackendClient,
  BackendClientCurry,
  CreateCalendar,
  CreateEvent,
  CreateFollower,
  CreateRSVP,
  DeAuthorize,
  EditCalendar,
  EditEvent,
  EditRSVP,
  EditUser,
  GetCalendars,
  GetEvents,
  GetFollowers,
  GetRSVPs,
  GetUser,
  Login,
  Logout,
  PublicCalendar,
  PublicEvent,
  PublicFollower,
  PublicRSVP,
  PublicSession,
  PublicUser,
  RegisterUser,
  RemoveCalendar,
  RemoveEvent,
  RemoveFollower,
  RemoveRSVP,
  RemoveUser,
  Subscribe,
  Unsubscribe
} from '../typings'

let instance: BackendClient | null = null

export const getInstance = (): BackendClient => {
  if (instance) {
    return instance
  }

  const client = axios.create({
    baseURL: '/api'
  })

  instance = {
    authorize: authorize(client),
    deAuthorize: deAuthorize(client),

    login: login(client),
    logout: logout(client),

    subscribe: subscribe(client),
    unsubscribe: unsubscribe(client),

    getUser: getUser(client),
    registerUser: registerUser(client),
    editUser: editUser(client),
    removeUser: removeUser(client),

    getCalendars: getCalendars(client),
    createCalendar: createCalendar(client),
    editCalendar: editCalendar(client),
    removeCalendar: removeCalendar(client),

    getFollowers: getFollowers(client),
    createFollower: createFollower(client),
    removeFollower: removeFollower(client),

    getEvents: getEvents(client),
    createEvent: createEvent(client),
    editEvent: editEvent(client),
    removeEvent: removeEvent(client),

    getRSVPs: getRSVPs(client),
    createRSVP: createRSVP(client),
    editRSVP: editRSVP(client),
    removeRSVP: removeRSVP(client)
  }

  return instance
}

const authorize: BackendClientCurry<Authorize> = (client) => (token) => {
  client.defaults.headers.session = token
}

const deAuthorize: BackendClientCurry<DeAuthorize> = (client) => () => {
  client.defaults.headers.session = ''
}

const login: BackendClientCurry<Login> = (client) => async (details) => {
  const response = await client.post<PublicSession>('/sessions', details)
  return response.data
}

const logout: BackendClientCurry<Logout> = (client) => async (token) => {
  await client.delete<void>(`/sessions/${token}`)
}

const subscribe: BackendClientCurry<Subscribe> = (client) => async (token) => {
  await client.post<void>('/notifications', { token })
}

const unsubscribe: BackendClientCurry<Unsubscribe> = (client) => async (token) => {
  await client.delete<void>(`/notifications/${token}`)
}

const getUser: BackendClientCurry<GetUser> = (client) => async (userId) => {
  const response = await client.get<PublicUser>(`/users/${userId}`)
  return response.data
}

const registerUser: BackendClientCurry<RegisterUser> = (client) => async (details) => {
  const response = await client.post<PublicUser>('/users', details)
  return response.data
}

const editUser: BackendClientCurry<EditUser> = (client) => async (userId, details) => {
  const response = await client.patch<PublicUser>(`/users/${userId}`, details)
  return response.data
}

const removeUser: BackendClientCurry<RemoveUser> = (client) => async (userId) => {
  await client.delete<void>(`/users/${userId}`)
}

const getCalendars: BackendClientCurry<GetCalendars> = (client) => async (params) => {
  const response = await client.get<PublicCalendar[]>('/calendars', { params })
  return response.data
}

const createCalendar: BackendClientCurry<CreateCalendar> = (client) => async (details) => {
  const response = await client.post<PublicCalendar>('/calendars', details)
  return response.data
}

const editCalendar: BackendClientCurry<EditCalendar> = (client) => async (calendarId, details) => {
  const response = await client.patch<PublicCalendar>(`/calendars/${calendarId}`, details)
  return response.data
}

const removeCalendar: BackendClientCurry<RemoveCalendar> = (client) => async (calendarId) => {
  await client.delete<void>(`/calendars/${calendarId}`)
}

const getFollowers: BackendClientCurry<GetFollowers> = (client) => async (calendarId, params) => {
  const response = await client.get<PublicFollower[]>(`/calendars/${calendarId}/followers`, { params })
  return response.data
}

const createFollower: BackendClientCurry<CreateFollower> = (client) => async (details) => {
  const response = await client.post<PublicFollower>('/followers', details)
  return response.data
}

const removeFollower: BackendClientCurry<RemoveFollower> = (client) => async (followerId) => {
  await client.delete<void>(`/followers/${followerId}`)
}

const getEvents: BackendClientCurry<GetEvents> = (client) => async (calendarId, params) => {
  const response = await client.get<PublicEvent[]>(`/calendars/${calendarId}/events`, { params })
  return response.data
}

const createEvent: BackendClientCurry<CreateEvent> = (client) => async (details) => {
  const response = await client.post<PublicEvent>('/events', details)
  return response.data
}

const editEvent: BackendClientCurry<EditEvent> = (client) => async (eventId, details) => {
  const response = await client.patch<PublicEvent>(`/events/${eventId}`, details)
  return response.data
}

const removeEvent: BackendClientCurry<RemoveEvent> = (client) => async (eventId) => {
  await client.delete<void>(`/events/${eventId}`)
}

const getRSVPs: BackendClientCurry<GetRSVPs> = (client) => async (params) => {
  const response = await client.get<PublicRSVP[]>('/rsvps', { params })
  return response.data
}

const createRSVP: BackendClientCurry<CreateRSVP> = (client) => async (details) => {
  const response = await client.post<PublicRSVP>('/rsvps', details)
  return response.data
}

const editRSVP: BackendClientCurry<EditRSVP> = (client) => async (rsvpId, details) => {
  const response = await client.patch<PublicRSVP>(`/rsvps/${rsvpId}`, details)
  return response.data
}

const removeRSVP: BackendClientCurry<RemoveRSVP> = (client) => async (rsvpId) => {
  await client.delete<void>(`/rsvps/${rsvpId}`)
}
