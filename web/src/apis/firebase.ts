import firebase from 'firebase'
import { firebaseCredentials } from '../config'
import { FirebaseClient, FirebaseClientCurry, GetToken } from '../typings'

let instance: FirebaseClient | null = null

export const getInstance = (): FirebaseClient => {
  if (instance) {
    return instance
  }

  const app = firebase.initializeApp(firebaseCredentials)
  const client = firebase.messaging(app)

  instance = {
    getToken: getToken(client)
  }

  return instance
}

const getToken: FirebaseClientCurry<GetToken> = (client) => async () => {
  const token = await client.getToken()
  return token
}
