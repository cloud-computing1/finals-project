import { FunctionalComponent, h } from 'preact'
import { Store } from './logic'
import { Routing } from './pages'

export const App: FunctionalComponent = () => (
  <Store>
    <Routing />
  </Store>
)
