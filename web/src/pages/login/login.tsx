import { FunctionalComponent, h } from 'preact'
import { Link, route } from 'preact-router'
import { StateUpdater, useContext, useEffect, useState } from 'preact/hooks'
import { Box, List, Page, Spaced } from '../../components'
import { sizes } from '../../config'
import { Context } from '../../logic'

const INITIAL_LOGIN_BUTTON_TEXT = 'Login'

export const Login: FunctionalComponent = () => {
  const [buttonText, setButtonText] = useState(INITIAL_LOGIN_BUTTON_TEXT)
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const { state, dispatch } = useContext(Context)

  const onSubmit = (event: Event) => {
    event.preventDefault()

    setLoading(true)
    dispatch({ type: 'LOGIN', payload: { email, password } })
  }

  const update = (updater: StateUpdater<string>) => (event: h.JSX.TargetedEvent<HTMLInputElement>) => updater((event.target as HTMLInputElement).value)

  useEffect(() => {
    if (loading && !state.Auth.loading) {
      if (!state.Auth.error && state.Auth.isLoggedIn) {
        setButtonText('✅')
        setTimeout(() => route('/calendars', true), 1000)
      } else {
        setButtonText('❌')
        setTimeout(() => {
          setButtonText(INITIAL_LOGIN_BUTTON_TEXT)
          setLoading(false)
        }, 1000)
      }
    }
  }, [loading, state.Auth])

  return (
    <Page justifyContent="center" alignItems="stretch" padding="0 40%">
      <Box margin={'0'} noShadow>
        <Box padding={`${sizes.xs} ${sizes.m} ${sizes.m} ${sizes.m}`}>
          <List component={({ children }) => <Spaced size={sizes.xxxs}>{children}</Spaced>}>
            <h1>🥷🏻 Login</h1>
            <label for="email">Email</label>
            <input value={email} onChange={update(setEmail)} id="email" type="text" placeholder="john.doe@calendary.com" />
            <label for="password">Password</label>
            <input value={password} onChange={update(setPassword)} id="password" type="password" placeholder="Password" />
            <input type="submit" value={buttonText} disabled={loading} onClick={onSubmit} />
            <Link href="/register">Did you mean to register instead?</Link>
          </List>
        </Box>
      </Box>
    </Page>
  )
}
