import { Fragment, FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { useContext, useEffect, useState } from 'preact/hooks'
import { Box, Event, Page } from '../../components'
import { constants } from '../../config'
import { Context } from '../../logic'
import { Loading } from '../loading'

interface Props {
  calendarId: string
}

export const CalendarEvents: FunctionalComponent<Props> = ({ calendarId }) => {
  const [skip, setSkip] = useState(0)
  const [loading, setLoading] = useState(true)
  const { state, dispatch } = useContext(Context)

  useEffect(() => {
    dispatch({ type: 'GET_EVENTS', payload: { calendarId, skip, limit: constants.DEFAULT_LIMIT } })
  }, [skip])

  useEffect(() => {
    if (loading) {
      if (state.Events.events.length === skip + constants.DEFAULT_LIMIT) {
        setSkip(oldSkip => oldSkip + constants.DEFAULT_LIMIT)
      } else {
        setLoading(false)
      }
    }
  }, [loading, state.Events.loading])

  const goToAdd = () => route(`/calendars/${calendarId}/events/create`)

  return (
    <Fragment>
      {loading
        ? <Loading />
        : (
          <Page justifyContent="flex-start" alignItems="stretch" padding="0 35%">
            {state.Events.events.map(event => <Event key={event.eventId} {...event} />)}
            <Box>
              <h1>➕ Want to add more?</h1>
              <p>Click here to do this</p>
              <button onClick={goToAdd}>Add</button>
            </Box>
          </Page>
          )
      }
    </Fragment>
  )
}
