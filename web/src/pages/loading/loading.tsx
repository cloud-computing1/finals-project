import { FunctionalComponent, h } from 'preact'
import { Animated, Box, Image, Page } from '../../components'
import { sizes } from '../../config'

export const Loading: FunctionalComponent = () => (
  <Page justifyContent="center" alignItems="center">
    <Box justifyContent="center" alignItems="center">
      <Animated animation={'rotate'}>
        <Image asset="gear" height={sizes.xxl} width={sizes.xxl} />
      </Animated>
      <h1 style={{ margin: `${sizes.xs} 0 0 0` }}>Loading...</h1>
    </Box>
  </Page>
)
