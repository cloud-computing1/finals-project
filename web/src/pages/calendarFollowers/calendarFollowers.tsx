import { Fragment, FunctionalComponent, h } from 'preact'
import { useContext, useEffect, useState } from 'preact/hooks'
import { Box, Page } from '../../components'
import { constants } from '../../config'
import { Context } from '../../logic'
import { Loading } from '../loading'

interface Props {
  calendarId: string
}

export const CalendarFollowers: FunctionalComponent<Props> = ({ calendarId }) => {
  const [skip, setSkip] = useState(0)
  const [loading, setLoading] = useState(true)
  const { state, dispatch } = useContext(Context)

  useEffect(() => {
    dispatch({ type: 'GET_FOLLOWERS', payload: { calendarId, skip, limit: constants.DEFAULT_LIMIT } })
  }, [skip])

  useEffect(() => {
    if (loading) {
      if (state.Followers.followers.length === skip + constants.DEFAULT_LIMIT) {
        setSkip(oldSkip => oldSkip + constants.DEFAULT_LIMIT)
      } else {
        setLoading(false)
      }
    }
  }, [loading, state.Followers.loading])

  return (
    <Fragment>
      {loading
        ? <Loading />
        : (
          <Page justifyContent="flex-start" alignItems="stretch" padding="0 35%">
            {state.Followers.followers.map(follower =>
              <Box key={follower.userId}>
                <p>{follower.userId}</p>
              </Box>
            )
            }
          </Page>
          )
      }
    </Fragment>
  )
}
