import { FunctionalComponent, h } from 'preact'
import { Article, List, Page } from '../../components'

export const Home: FunctionalComponent = () => {
  return (
    <Page alignItems="stretch" padding="0 35%">
      <List>
        <Article title="Welcome 👋" content="Welcome to my presentation" caption={new Date().toDateString()} />
      </List>
    </Page>
  )
}
