import { Fragment, FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { useContext, useEffect } from 'preact/hooks'
import { Context } from '../../logic'

export const Logout: FunctionalComponent = () => {
  const { state, dispatch } = useContext(Context)

  dispatch({ type: 'LOGOUT' })

  useEffect(() => {
    if (!state.Auth.isLoggedIn) {
      route('/', true)
    }
  }, [state.Auth])

  return (
    <Fragment />
  )
}
