import { Fragment, FunctionalComponent, h } from 'preact';
import { route } from 'preact-router';

export const RedirectToHome: FunctionalComponent = () => {
  route('/', false)

  return <Fragment/>
};
