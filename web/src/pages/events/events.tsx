import { FunctionalComponent, h } from 'preact'
import { Box, Image, Page } from '../../components'
import { sizes } from '../../config'

export const Events: FunctionalComponent = () => (
  <Page justifyContent="center" alignItems="stretch" padding="0 35%">
    <Box justifyContent="center" alignItems="center">
      <Image asset="exclamation" height={sizes.xxl} width={sizes.xxl} />
      <h1 style={{ margin: `${sizes.xs} 0 0 0` }}>Not implemented yet ⚙️</h1>
    </Box>
  </Page>
)
