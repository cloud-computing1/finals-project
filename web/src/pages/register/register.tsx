import { FunctionalComponent, h } from 'preact'
import { Link, route } from 'preact-router'
import { StateUpdater, useContext, useEffect, useState } from 'preact/hooks'
import { Box, List, Page, Spaced } from '../../components'
import { sizes } from '../../config'
import { Context } from '../../logic'

const INITIAL_REGISTER_BUTTON_TEXT = 'Register'

export const Register: FunctionalComponent = () => {
  const [buttonText, setButtonText] = useState(INITIAL_REGISTER_BUTTON_TEXT)
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const { state, dispatch } = useContext(Context)

  const onSubmit = (event: Event) => {
    event.preventDefault()

    setLoading(true)
    dispatch({ type: 'REGISTER', payload: { email, password, firstName, lastName } })
  }

  const update = (updater: StateUpdater<string>) => (event: h.JSX.TargetedEvent<HTMLInputElement>) => updater((event.target as HTMLInputElement).value)

  useEffect(() => {
    if (loading && !state.Auth.loading) {
      if (!state.Auth.error) {
        setButtonText('✅')
        setTimeout(() => route('/login', true), 1000)
      } else {
        setButtonText('❌')
        setTimeout(() => {
          setButtonText(INITIAL_REGISTER_BUTTON_TEXT)
          setLoading(false)
        }, 1000)
      }
    }
  }, [loading, state.Auth])

  return (
    <Page justifyContent="center" alignItems="stretch" padding="0 40%">
      <Box margin={'0'} noShadow>
        <Box padding={`${sizes.xs} ${sizes.m} ${sizes.m} ${sizes.m}`}>
          <List component={({ children }) => <Spaced size={sizes.xxxs}>{children}</Spaced>}>
            <h1>🥷🏻 Register</h1>
            <label for="email">Email</label>
            <input value={email} onChange={update(setEmail)} id="email" type="text" placeholder="john.doe@calendary.com" />
            <label for="password">Password</label>
            <input value={password} onChange={update(setPassword)} id="password" type="password" placeholder="Password" />
            <label for="firstName">First name</label>
            <input value={firstName} onChange={update(setFirstName)} id="firstName" type="text" placeholder="John" />
            <label for="lastName">Last name</label>
            <input value={lastName} onChange={update(setLastName)} id="lastName" type="text" placeholder="Doe" />
            <input type="submit" value={buttonText} disabled={loading} onClick={onSubmit} />
            <Link href="/login">Did you mean to login instead?</Link>
          </List>
        </Box>
      </Box>
    </Page>
  )
}
