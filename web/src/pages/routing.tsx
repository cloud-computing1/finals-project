import { Fragment, FunctionalComponent, h } from 'preact'
import { Route, Router } from 'preact-router'
import { useContext } from 'preact/hooks'
import { Header, ProtectedRoute } from '../components'
import { Context } from '../logic'
import { CalendarEvents } from './calendarEvents'
import { CalendarFollowers } from './calendarFollowers'
import { Calendars } from './calendars'
import { CreateCalendar } from './createCalendar'
import { CreateEvent } from './createEvent'
import { EditCalendar } from './editCalendar'
import { Events } from './events'
import { Home } from './home'
import { Loading } from './loading'
import { Login } from './login'
import { Logout } from './logout'
import { RedirectToHome } from './redirectToHome'
import { Register } from './register'

export const Routing: FunctionalComponent = () => {
  const { state } = useContext(Context)

  return (
    <Fragment>
      {!state.isLoaded
        ? <Loading />
        : (
          <Fragment>
            <Header />
            <Router>
              <Route path="/" component={Home} />
              <Route path="/register" component={Register} />
              <Route path="/login" component={Login} />
              <ProtectedRoute path="/logout" component={Logout} />
              <ProtectedRoute path="/calendars" component={Calendars} />
              <ProtectedRoute path="/calendars/create" component={CreateCalendar} />
              <ProtectedRoute path="/calendars/:calendarId/edit" component={EditCalendar} />
              <ProtectedRoute path="/calendars/:calendarId/followers" component={CalendarFollowers} />
              <ProtectedRoute path="/calendars/:calendarId/events" component={CalendarEvents} />
              <ProtectedRoute path="/calendars/:calendarId/events/create" component={CreateEvent} />
              <ProtectedRoute path="/events" component={Events} />
              <RedirectToHome default />
            </Router>
          </Fragment>
          )
      }
    </Fragment>
  )
}
