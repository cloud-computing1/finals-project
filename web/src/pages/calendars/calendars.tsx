import { Fragment, FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { useContext, useEffect, useState } from 'preact/hooks'
import { Box, Calendar, Page } from '../../components'
import { constants } from '../../config'
import { Context } from '../../logic'
import { Loading } from '../loading'

export const Calendars: FunctionalComponent = () => {
  const [skip, setSkip] = useState(0)
  const [loading, setLoading] = useState(true)
  const { state, dispatch } = useContext(Context)

  useEffect(() => {
    dispatch({ type: 'RESET_CALENDARS' })
  }, [])

  useEffect(() => {
    dispatch({ type: 'GET_CALENDARS', payload: { skip, limit: constants.DEFAULT_LIMIT } })
  }, [skip])

  useEffect(() => {
    if (loading) {
      if (state.Calendars.calendars.length === skip + constants.DEFAULT_LIMIT) {
        setSkip(oldSkip => oldSkip + constants.DEFAULT_LIMIT)
      } else {
        setLoading(false)
      }
    }
  }, [loading, state.Calendars.loading])

  const goToAdd = () => route('/calendars/create')

  return (
    <Fragment>
      {loading
        ? <Loading />
        : (
          <Page justifyContent="flex-start" alignItems="stretch" padding="0 35%">
            {state.Calendars.calendars.map(calendar => <Calendar key={calendar.calendarId} {...calendar} />)}
            <Box>
              <h1>➕ Want to add more?</h1>
              <p>Click here to do this</p>
              <button onClick={goToAdd}>Add</button>
            </Box>
          </Page>
          )
      }
    </Fragment>
  )
}
