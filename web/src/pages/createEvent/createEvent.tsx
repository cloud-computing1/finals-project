import { FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { StateUpdater, useContext, useEffect, useState } from 'preact/hooks'
import { Box, List, Page, Spaced } from '../../components'
import { sizes } from '../../config'
import { Context } from '../../logic'

const INITIAL_BUTTON_TEXT = 'Add'

interface Props {
  calendarId: string
}

export const CreateEvent: FunctionalComponent<Props> = ({ calendarId }) => {
  const { state, dispatch } = useContext(Context)
  const [buttonText, setButtonText] = useState(INITIAL_BUTTON_TEXT)
  const [loading, setLoading] = useState(false)
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [startDate, setStartDate] = useState('')
  const [endDate, setEndDate] = useState('')

  const onSubmit = (event: Event) => {
    event.preventDefault()

    setLoading(true)
    dispatch({ type: 'CREATE_EVENT', payload: { calendarId, name, description, startDate, endDate } })
  }

  const update = (updater: StateUpdater<string>) => (event: h.JSX.TargetedEvent<HTMLInputElement>) => updater((event.target as HTMLInputElement).value)

  useEffect(() => {
    if (loading && !state.Events.loading) {
      if (!state.Events.error) {
        setButtonText('✅')
        setTimeout(() => route(`/calendars/${calendarId}/events`, true), 1000)
      } else {
        setButtonText('❌')
        setTimeout(() => {
          setButtonText(INITIAL_BUTTON_TEXT)
          setLoading(false)
        }, 1000)
      }
    }
  }, [loading, state.Events.error, state.Events.loading])

  return (
    <Page justifyContent="flex-start" alignItems="stretch" padding="0 35%">
      <Box margin={'0'} noShadow>
        <Box padding={`${sizes.xs} ${sizes.m} ${sizes.m} ${sizes.m}`}>
          <List component={({ children }) => <Spaced size={sizes.xxxs}>{children}</Spaced>}>
            <h1>📝 Add event</h1>
            <label for="name">Name</label>
            <input value={name} onChange={update(setName)} id="name" type="text" placeholder="My event" />
            <label for="description">Description</label>
            <input value={description} onChange={update(setDescription)} id="description" type="textarea" placeholder="Description" />
            <label for="startDate">Start date</label>
            <input value={startDate} onChange={update(setStartDate)} id="startDate" type="datetime" placeholder={new Date(Date.now() + 3600000).toISOString()} />
            <label for="endDate">End date</label>
            <input value={endDate} onChange={update(setEndDate)} id="endDate" type="datetime" placeholder={new Date(Date.now() + 3600000 * 2).toISOString()} />
            <input type="submit" value={buttonText} disabled={loading} onClick={onSubmit} />
          </List>
        </Box>
      </Box>
    </Page>
  )
}
