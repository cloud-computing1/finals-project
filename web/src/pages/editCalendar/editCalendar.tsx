import { FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { StateUpdater, useContext, useEffect, useMemo, useState } from 'preact/hooks'
import { Box, List, Page, Spaced } from '../../components'
import { sizes } from '../../config'
import { Context } from '../../logic'

const INITIAL_BUTTON_TEXT = 'Edit'

interface Props {
  calendarId: string
}

export const EditCalendar: FunctionalComponent<Props> = ({ calendarId }) => {
  const { state, dispatch } = useContext(Context)
  const calendar = useMemo(() => state.Calendars.calendars.find(calendar => calendar.calendarId === calendarId), [state.Calendars.calendars])
  const [buttonText, setButtonText] = useState(INITIAL_BUTTON_TEXT)
  const [loading, setLoading] = useState(false)
  const [name, setName] = useState(calendar?.name ?? '')
  const [description, setDescription] = useState(calendar?.description ?? '')
  const [visibility, setVisibility] = useState(calendar?.visibility ?? '')

  const onSubmit = (event: Event) => {
    event.preventDefault()

    setLoading(true)
    dispatch({ type: 'EDIT_CALENDAR', payload: { calendarId, name, description, visibility } })
  }

  const update = (updater: StateUpdater<string>) => (event: h.JSX.TargetedEvent<HTMLInputElement>) => updater((event.target as HTMLInputElement).value)
  const updateSelect = (updater: StateUpdater<string>) => (event: h.JSX.TargetedEvent<HTMLSelectElement>) => updater((event.target as HTMLSelectElement).value)

  useEffect(() => {
    if (loading && !state.Calendars.loading) {
      if (!state.Calendars.error) {
        setButtonText('✅')
        setTimeout(() => route('/calendars', true), 1000)
      } else {
        setButtonText('❌')
        setTimeout(() => {
          setButtonText(INITIAL_BUTTON_TEXT)
          setLoading(false)
        }, 1000)
      }
    }
  }, [loading, state.Calendars.error, state.Calendars.loading])

  if (!calendar) {
    route('/', true)

    return null
  }

  return (
    <Page justifyContent="flex-start" alignItems="stretch" padding="0 35%">
      <Box margin={'0'} noShadow>
        <Box padding={`${sizes.xs} ${sizes.m} ${sizes.m} ${sizes.m}`}>
          <List component={({ children }) => <Spaced size={sizes.xxxs}>{children}</Spaced>}>
            <h1>📆 Edit calendar</h1>
            <label for="name">Name</label>
            <input value={name} onChange={update(setName)} id="name" type="text" placeholder="My calendar" />
            <label for="description">Description</label>
            <input value={description} onChange={update(setDescription)} id="description" type="textarea" placeholder="Description" />
            <label for="visibility">Visibility</label>
            <select value={visibility} onChange={updateSelect(setVisibility)} id="visibility">
              <option value="private">Private</option>
              <option value="public">Public</option>
            </select>
            <input type="submit" value={buttonText} disabled={loading} onClick={onSubmit} />
          </List>
        </Box>
      </Box>
    </Page>
  )
}
