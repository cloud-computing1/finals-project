import firebase from 'firebase'

export type RegisterServiceWorker = () => Promise<void>;
export type GetToken = () => Promise<string>;

export type FirebaseClient = {
  getToken: GetToken;
};

export type FirebaseClientCurry<T = RegisterServiceWorker | GetToken> = (client: firebase.messaging.Messaging) => T;
