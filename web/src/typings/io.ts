/* eslint-disable no-unused-vars */

export type PublicEntity = {
  [key: string]: unknown;
};

export enum CalendarVisibility {
  Public = 'public',
  Private = 'private',
}

export enum RSVPStatus {
  None = 'none',
  Maybe = 'maybe',
  Going = 'going',
  NotGoing = 'notGoing',
}

export type PublicSession = PublicEntity & {
  userId: string;
  session: string;
};

export type PublicRSVP = PublicEntity & {
  rsvpId: string;
  userId: string;
  eventId: string;
  status: RSVPStatus;
  createDate: Date;
  updateDate: Date;
};

export type PublicUser = PublicEntity & {
  userId: string;
  email: string;
  firstName: string;
  lastName: string;
  createDate: Date;
  updateDate: Date;
};

export type PublicUserWithPassword = PublicUser & {
  password: string;
};

export type PublicFollower = PublicEntity & {
  userId: string;
  calendarId: string;
  createDate: Date;
  updateDate: Date;
};

export type PublicEvent = PublicEntity & {
  eventId: string;
  ownerUserId: string;
  ownerCalendarId: string;
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  createDate: Date;
  updateDate: Date;
};

export type PublicCalendar = PublicEntity & {
  calendarId: string;
  ownerUserId: string;
  name: string;
  description: string;
  visibility: CalendarVisibility;
  createDate: Date;
  updateDate: Date;
};


