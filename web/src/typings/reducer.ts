export type AbstractAction = { type: string; payload?: Record<string, unknown> };
