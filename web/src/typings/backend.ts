import { AxiosInstance } from 'axios'
import {
  PublicCalendar,
  PublicEvent,
  PublicFollower,
  PublicRSVP,
  PublicSession,
  PublicUser,
  PublicUserWithPassword
} from './io'

type SkipLimitQuery = {
  skip?: number;
  limit?: number;
  query?: string;
};

export type Authorize = (token: string) => void;
export type DeAuthorize = () => void;

export type Login = (details: Pick<PublicUserWithPassword, 'email' | 'password'>) => Promise<PublicSession>;
export type Logout = (token: string) => Promise<void>;

export type GetUser = (userId: Pick<PublicUserWithPassword, 'userId'>) => Promise<PublicUser>;
export type RegisterUser = (
  details: Omit<PublicUserWithPassword, 'userId' | 'createDate' | 'updateDate'>
) => Promise<PublicUser>;
export type EditUser = (
  userId: string,
  details: Partial<Omit<PublicUserWithPassword, 'userId' | 'createDate' | 'updateDate'>>
) => Promise<PublicUser>;
export type RemoveUser = (userId: string) => Promise<void>;

export type Subscribe = (token: string) => Promise<void>;
export type Unsubscribe = (token: string) => Promise<void>;

export type GetCalendars = (params: SkipLimitQuery) => Promise<PublicCalendar[]>;
export type CreateCalendar = (
  details: Omit<PublicCalendar, 'calendarId' | 'createDate' | 'updateDate'>
) => Promise<PublicCalendar>;
export type EditCalendar = (
  calendarId: string,
  details: Partial<Omit<PublicCalendar, 'calendarId' | 'createDate' | 'updateDate'>>
) => Promise<PublicCalendar>;
export type RemoveCalendar = (calendarId: string) => Promise<void>;

export type GetFollowers = (calendarId: string, params: SkipLimitQuery) => Promise<PublicFollower[]>;
export type CreateFollower = (
  details: Omit<PublicFollower, 'followerId' | 'createDate' | 'updateDate'>
) => Promise<PublicFollower>;
export type RemoveFollower = (followerId: string) => Promise<void>;

export type GetEvents = (calendarId: string, params: SkipLimitQuery) => Promise<PublicEvent[]>;
export type CreateEvent = (details: Omit<PublicEvent, 'eventId' | 'createDate' | 'updateDate'>) => Promise<PublicEvent>;
export type EditEvent = (
  eventId: string,
  details: Partial<Omit<PublicEvent, 'eventId' | 'createDate' | 'updateDate'>>
) => Promise<PublicEvent>;
export type RemoveEvent = (eventId: string) => Promise<void>;

export type GetRSVPs = (params: SkipLimitQuery) => Promise<PublicRSVP[]>;
export type CreateRSVP = (details: Omit<PublicRSVP, 'rsvpId' | 'createDate' | 'updateDate'>) => Promise<PublicRSVP>;
export type EditRSVP = (
  rsvpId: string,
  details: Partial<Omit<PublicRSVP, 'rsvpId' | 'eventId' | 'createDate' | 'updateDate'>>
) => Promise<PublicRSVP>;
export type RemoveRSVP = (rsvpId: string) => Promise<void>;

export type BackendClient = {
  authorize: Authorize;
  deAuthorize: DeAuthorize;

  login: Login;
  logout: Logout;

  getUser: GetUser;
  registerUser: RegisterUser;
  editUser: EditUser;
  removeUser: RemoveUser;

  subscribe: Subscribe;
  unsubscribe: Unsubscribe;

  getCalendars: GetCalendars;
  createCalendar: CreateCalendar;
  editCalendar: EditCalendar;
  removeCalendar: RemoveCalendar;

  getFollowers: GetFollowers;
  createFollower: CreateFollower;
  removeFollower: RemoveFollower;

  getEvents: GetEvents;
  createEvent: CreateEvent;
  editEvent: EditEvent;
  removeEvent: RemoveEvent;

  getRSVPs: GetRSVPs;
  createRSVP: CreateRSVP;
  editRSVP: EditRSVP;
  removeRSVP: RemoveRSVP;
};

export type BackendClientCurry<
  T =
    | Authorize
    | DeAuthorize
    | Login
    | Logout
    | GetUser
    | RegisterUser
    | EditUser
    | RemoveUser
    | Subscribe
    | Unsubscribe
    | GetCalendars
    | CreateCalendar
    | EditCalendar
    | RemoveCalendar
    | GetFollowers
    | CreateFollower
    | RemoveFollower
    | GetEvents
    | CreateEvent
    | EditEvent
    | RemoveEvent
    | GetRSVPs
    | CreateRSVP
    | EditRSVP
    | RemoveRSVP
> = (client: AxiosInstance) => T;
