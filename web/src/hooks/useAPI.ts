import { useMemo } from 'preact/hooks'
import { api } from '../apis'

export const useAPI = () => useMemo(() => api.getInstance(), [])
