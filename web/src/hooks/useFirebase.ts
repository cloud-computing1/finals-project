import { useMemo } from 'preact/hooks'
import { firebase } from '../apis'

export const useFirebase = () => useMemo(() => firebase.getInstance(), [])
