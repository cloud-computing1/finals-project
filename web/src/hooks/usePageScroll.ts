import { useEffect, useRef } from 'preact/hooks'

export const usePageScroll = (callback: () => void) => {
  const timer = useRef<number | null>()

  useEffect(() => {
    const handler = (event: Event) => {
      const element = event.target as HTMLElement

      if (timer.current) {
        clearTimeout(timer.current)
      }

      if ((element.scrollTop + element.clientHeight) / element.scrollHeight > 0.8) {
        timer.current = window.setTimeout(() => {
          timer.current = null
          callback()
        }, 250)
      }
    }

    document.body.addEventListener('scroll', handler, true)

    return () => {
      document.body.removeEventListener('scroll', handler)
    }
  }, [])
}
