import { Reducer, useCallback, useReducer } from 'preact/hooks'

type SagaInterceptor<State, Actions, AllActions> = (saga: {
  state: State;
  action: Actions;
  dispatch: (action: AllActions) => void;
}) => void;

export const useReducerWithSaga = <State, Actions>(
  reducer: Reducer<State, Actions>,
  sagas: SagaInterceptor<State, Actions, Actions>,
  initialState: State
): [State, (action: Actions) => void] => {
  const [state, dispatch] = useReducer<State, Actions>(reducer, initialState)

  const sagaInterceptor = useCallback((action: Actions) => {
    sagas({ action, state, dispatch })
    dispatch(action)
  }, [sagas, state])

  return [state, sagaInterceptor]
}
