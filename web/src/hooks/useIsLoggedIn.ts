import { useContext } from 'preact/hooks'
import { Context } from '../logic'

export const useIsLoggedIn = () => {
  const { state } = useContext(Context)

  return state.Auth.isLoggedIn
}
