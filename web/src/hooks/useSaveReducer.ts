import equal from 'fast-deep-equal/es6'
import { useEffect, useRef, useState } from 'preact/hooks'
import { AbstractAction } from '../typings'

export type PopulatedState<State = Record<string, unknown>> = State;
export type PopulateAction<State = Record<string, unknown>> = AbstractAction & { type: 'POPULATE'; payload: PopulatedState<State> };
export type AfterPopulateAction = AbstractAction & { type: 'AFTER_POPULATE' };
export type Dispatch = (action: PopulateAction | AfterPopulateAction) => void;

export const useSaveReducer = <State>(state: State, dispatch: Dispatch, defaults: State) => {
  const previousState = useRef<State>(null)
  const [loaded, setIsLoaded] = useState(false)
  const [populated, setPopulated] = useState(false)

  useEffect(() => {
    try {
      const state = Object.keys(defaults).reduce((acc, key) => {
        const item = window.localStorage.getItem(key)

        if (!item) {
          return acc
        }

        acc[key] = JSON.parse(item)

        return acc
      }, {} as PopulatedState)

      dispatch({ type: 'POPULATE', payload: state })
      setIsLoaded(true)
    } catch {
      console.error('Error loading initial state, resetting state')

      Object.entries(defaults).forEach(([key, value]) => window.localStorage.setItem(key, JSON.stringify(value)))

      dispatch({ type: 'POPULATE', payload: defaults as PopulatedState })
    }
  }, [])

  useEffect(() => {
    if (loaded && !equal(previousState.current, state)) {
      previousState.current = state

      Object.entries(state).forEach(([key, value]) => window.localStorage.setItem(key, JSON.stringify(value ?? {})))
    }

    if (loaded && !populated) {
      dispatch({ type: 'AFTER_POPULATE' })
      setPopulated(true)
    }
  }, [loaded, state])
}
