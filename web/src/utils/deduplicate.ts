export const deduplicate = <T>(array: T[], field: keyof T): T[] => {
  return array.filter((a, indexA, array) => !array.find((b, indexB) => indexA < indexB && a[field] === b[field]))
}
