import { h } from 'preact'
import { colors, sizes } from '../config'

export interface FlexStyle {
  flex?: number;
  direction?: 'row' | 'column';
  justifyContent?: 'center' | 'flex-start' | 'flex-end' | 'stretch' | 'space-around' | 'space-between';
  alignItems?: 'center' | 'flex-start' | 'flex-end' | 'stretch' | 'space-around' | 'space-between';

  height?: string;
  maxHeight?: string;
  width?: string;
  maxWidth?: string;

  margin?: string;
  padding?: string;
  overflow?: string;
  noShadow?: boolean;
}

export const flexStyle = ({
  flex,
  direction,
  justifyContent,
  alignItems,
  height,
  maxHeight,
  width,
  maxWidth,
  margin,
  padding,
  overflow,
  noShadow
}: Partial<FlexStyle>): h.JSX.CSSProperties => ({
  display: 'flex',

  'flex-direction': direction || 'column',
  ...(flex && { flex }),
  ...(justifyContent && { justifyContent }),
  ...(alignItems && { alignItems }),

  ...(height && { height }),
  ...(maxHeight && { maxHeight }),
  ...(width && { width }),
  ...(maxWidth && { maxWidth }),

  margin: margin || sizes.xs,
  padding: padding || sizes.xs,
  ...(overflow && { overflow }),
  ...(!noShadow && { boxShadow: `0 0 ${sizes.s} ${colors.neutral5}` })
})
