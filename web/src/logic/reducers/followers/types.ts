import { AbstractAction, PublicFollower } from '../../../typings'

export type GetFollowers = AbstractAction & {
  type: 'GET_FOLLOWERS';
  payload: { calendarId: string; skip?: number; limit?: number; query?: string };
};
export type GetFollowersFailed = AbstractAction & { type: 'GET_FOLLOWERS_FAILED' };
export type GetFollowersSuccessful = AbstractAction & {
  type: 'GET_FOLLOWERS_SUCCESSFUL';
  payload: { followers: PublicFollower[] };
};

export type Actions = GetFollowers | GetFollowersFailed | GetFollowersSuccessful;

export type State = {
  loading: boolean;
  error: boolean;
  followers: PublicFollower[];
};
