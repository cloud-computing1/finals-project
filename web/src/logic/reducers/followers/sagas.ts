import { api } from '../../../apis'
import { deduplicate } from '../../../utils'
import { SagaDispatcher } from '../../types'
import { Actions, GetFollowers, State } from './types'

export const getFollowers: SagaDispatcher<State, GetFollowers, Actions> = async ({ state, action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    const data = await apiInstance.getFollowers(action.payload.calendarId, {
      skip: action.payload.skip,
      limit: action.payload.limit
    })

    dispatch({ type: 'GET_FOLLOWERS_SUCCESSFUL', payload: { followers: deduplicate(state.followers.concat(data), 'userId') } })
  } catch (err) {
    dispatch({ type: 'GET_FOLLOWERS_FAILED' })
  }
}
