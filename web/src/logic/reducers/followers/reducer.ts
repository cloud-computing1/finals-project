import { Reducer } from 'preact/hooks'
import { SagaDispatcher } from '../../types'
import { Actions } from '../main/types'
import { getFollowers } from './sagas'
import { GetFollowers, State } from './types'

export const initialState: State = {
  loading: false,
  error: false,
  followers: []
}

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'GET_FOLLOWERS':
      return {
        ...state,
        error: false,
        loading: true
      }
    case 'GET_FOLLOWERS_FAILED':
      return {
        ...state,
        loading: true,
        error: false
      }
    case 'GET_FOLLOWERS_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false,
        followers: action.payload.followers
      }
    default:
      return state
  }
}

export const saga: SagaDispatcher<State, Actions, Actions> = ({ state, action, dispatch }) => {
  switch (action.type) {
    case 'GET_FOLLOWERS':
      return getFollowers({ state, action: action as GetFollowers, dispatch })
    default:
      return null
  }
}
