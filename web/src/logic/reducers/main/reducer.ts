import { Reducer } from 'preact/hooks'
import { SagaDispatcher } from '../../types'
import * as auth from '../auth'
import * as calendars from '../calendars'
import * as events from '../events'
import * as followers from '../followers'
import { Actions, State } from './types'

export const initialState: State = {
  isLoaded: false,
  isPopulated: false,
  Auth: auth.initialState,
  Calendars: calendars.initialState,
  Followers: followers.initialState,
  Events: events.initialState
}

export const mainReducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'POPULATE':
      return {
        ...state,
        Auth: auth.reducer(state.Auth, { type: 'POPULATE', payload: action.payload.Auth as auth.State }),
        Calendars: calendars.reducer(state.Calendars, { type: 'POPULATE', payload: action.payload.Calendars as calendars.State }),
        Followers: followers.reducer(state.Followers, { type: 'POPULATE', payload: action.payload.Followers as followers.State }),
        Events: events.reducer(state.Events, { type: 'POPULATE', payload: action.payload.Events as events.State }),
        isPopulated: true,
        isLoaded: true
      }
    default:
      return {
        ...state,
        Auth: auth.reducer(state.Auth, action),
        Calendars: calendars.reducer(state.Calendars, action),
        Followers: followers.reducer(state.Followers, action),
        Events: events.reducer(state.Events, action)
      }
  }
}

export const mainSagaDispatcher: SagaDispatcher<State, Actions, Actions> = ({ action, state: { Auth, Calendars, Followers, Events }, dispatch }) => {
  auth.saga({ action, state: Auth, dispatch })
  calendars.saga({ action, state: Calendars, dispatch })
  followers.saga({ action, state: Followers, dispatch })
  events.saga({ action, state: Events, dispatch })
}
