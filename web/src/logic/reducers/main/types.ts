import { AfterPopulateAction, PopulateAction } from '../../../hooks'
import * as auth from '../auth'
import * as calendars from '../calendars'
import * as events from '../events'
import * as followers from '../followers'

export type State = {
  isLoaded: boolean;
  isPopulated: boolean;
  Auth: auth.State;
  Calendars: calendars.State;
  Followers: followers.State;
  Events: events.State;
};

export type Actions =
  | PopulateAction
  | AfterPopulateAction
  | auth.Actions
  | calendars.Actions
  | followers.Actions
  | events.Actions;
