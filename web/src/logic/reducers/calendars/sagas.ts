import { api } from '../../../apis'
import { deduplicate } from '../../../utils'
import { SagaDispatcher } from '../../types'
import { Actions, CreateCalendar, EditCalendar, GetCalendars, State } from './types'

export const getCalendars: SagaDispatcher<State, GetCalendars, Actions> = async ({ state, action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    const data = await apiInstance.getCalendars(action.payload)

    dispatch({ type: 'GET_CALENDARS_SUCCESSFUL', payload: { calendars: deduplicate(state.calendars.concat(data), 'calendarId') } })
  } catch (err) {
    dispatch({ type: 'GET_CALENDARS_FAILED' })
  }
}

export const editCalendar: SagaDispatcher<State, EditCalendar, Actions> = async ({ action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    await apiInstance.editCalendar(action.payload.calendarId, {
      name: action.payload.name,
      description: action.payload.description,
      visibility: action.payload.visibility
    })

    dispatch({ type: 'EDIT_CALENDAR_SUCCESSFUL' })
  } catch (err) {
    dispatch({ type: 'EDIT_CALENDAR_FAILED' })
  }
}

export const createCalendar: SagaDispatcher<State, CreateCalendar, Actions> = async ({ action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    await apiInstance.createCalendar(action.payload)

    dispatch({ type: 'CREATE_CALENDAR_SUCCESSFUL' })
  } catch (err) {
    dispatch({ type: 'CREATE_CALENDAR_FAILED' })
  }
}
