import { AbstractAction, PublicCalendar } from '../../../typings'

export type ResetCalendars = AbstractAction & { type: 'RESET_CALENDARS' };
export type GetCalendars = AbstractAction & {
  type: 'GET_CALENDARS';
  payload: { skip?: number; limit?: number; query?: string };
};
export type GetCalendarsFailed = AbstractAction & { type: 'GET_CALENDARS_FAILED' };
export type GetCalendarsSuccessful = AbstractAction & {
  type: 'GET_CALENDARS_SUCCESSFUL';
  payload: { calendars: PublicCalendar[] };
};
export type EditCalendar = AbstractAction & {
  type: 'EDIT_CALENDAR';
  payload: { calendarId: string; name: string; description: string; visibility: string };
};
export type EditCalendarFailed = AbstractAction & { type: 'EDIT_CALENDAR_FAILED' };
export type EditCalendarSuccessful = AbstractAction & { type: 'EDIT_CALENDAR_SUCCESSFUL' };

export type CreateCalendar = AbstractAction & {
  type: 'CREATE_CALENDAR';
  payload: { name: string; description: string; visibility: string };
};
export type CreateCalendarFailed = AbstractAction & { type: 'CREATE_CALENDAR_FAILED' };
export type CreateCalendarSuccessful = AbstractAction & { type: 'CREATE_CALENDAR_SUCCESSFUL' };

export type Actions =
  | ResetCalendars
  | GetCalendars
  | GetCalendarsFailed
  | GetCalendarsSuccessful
  | EditCalendar
  | EditCalendarFailed
  | EditCalendarSuccessful
  | CreateCalendar
  | CreateCalendarFailed
  | CreateCalendarSuccessful;

export type State = {
  loading: boolean;
  error: boolean;
  calendars: PublicCalendar[];
};
