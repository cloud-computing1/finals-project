import { Reducer } from 'preact/hooks'
import { SagaDispatcher } from '../../types'
import { Actions } from '../main/types'
import { createCalendar, editCalendar, getCalendars } from './sagas'
import { CreateCalendar, EditCalendar, GetCalendars, State } from './types'

export const initialState: State = {
  loading: false,
  error: false,
  calendars: []
}

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'RESET_CALENDARS':
      return {
        ...state,
        calendars: []
      }
    case 'GET_CALENDARS':
    case 'EDIT_CALENDAR':
    case 'CREATE_CALENDAR':
      return {
        ...state,
        error: false,
        loading: true
      }
    case 'GET_CALENDARS_FAILED':
    case 'EDIT_CALENDAR_FAILED':
    case 'CREATE_CALENDAR_FAILED':
      return {
        ...state,
        loading: false,
        error: true
      }
    case 'GET_CALENDARS_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false,
        calendars: action.payload.calendars
      }
    case 'EDIT_CALENDAR_SUCCESSFUL':
    case 'CREATE_CALENDAR_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false
      }
    default:
      return state
  }
}

export const saga: SagaDispatcher<State, Actions, Actions> = ({ state, action, dispatch }) => {
  switch (action.type) {
    case 'GET_CALENDARS':
      return getCalendars({ state, action: action as GetCalendars, dispatch })
    case 'EDIT_CALENDAR':
      return editCalendar({ state, action: action as EditCalendar, dispatch })
    case 'CREATE_CALENDAR':
      return createCalendar({ state, action: action as CreateCalendar, dispatch })
    default:
      return null
  }
}
