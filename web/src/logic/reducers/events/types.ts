import { AbstractAction, PublicEvent } from '../../../typings'

export type ResetEvents = AbstractAction & { type: 'RESET_EVENTS' };
export type GetEvents = AbstractAction & {
  type: 'GET_EVENTS';
  payload: { calendarId: string; skip?: number; limit?: number; query?: string };
};
export type GetEventsFailed = AbstractAction & { type: 'GET_EVENTS_FAILED' };
export type GetEventsSuccessful = AbstractAction & {
  type: 'GET_EVENTS_SUCCESSFUL';
  payload: { events: PublicEvent[] };
};

export type EditEvent = AbstractAction & {
  type: 'EDIT_EVENT';
  payload: { eventId: string; name?: string; description?: string; startDate?: string; endDate?: string };
};
export type EditEventFailed = AbstractAction & { type: 'EDIT_EVENT_FAILED' };
export type EditEventSuccessful = AbstractAction & { type: 'EDIT_EVENT_SUCCESSFUL' };

export type CreateEvent = AbstractAction & {
  type: 'CREATE_EVENT';
  payload: { calendarId: string; name?: string; description?: string; startDate?: string; endDate?: string };
};
export type CreateEventFailed = AbstractAction & { type: 'CREATE_EVENT_FAILED' };
export type CreateEventSuccessful = AbstractAction & { type: 'CREATE_EVENT_SUCCESSFUL' };

export type Actions =
  | ResetEvents
  | GetEvents
  | GetEventsFailed
  | GetEventsSuccessful
  | EditEvent
  | EditEventFailed
  | EditEventSuccessful
  | CreateEvent
  | CreateEventFailed
  | CreateEventSuccessful;

export type State = {
  loading: boolean;
  error: boolean;
  events: PublicEvent[];
};
