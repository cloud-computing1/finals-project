import { api } from '../../../apis'
import { deduplicate } from '../../../utils'
import { SagaDispatcher } from '../../types'
import { Actions, CreateEvent, EditEvent, GetEvents, State } from './types'

export const getEvents: SagaDispatcher<State, GetEvents, Actions> = async ({ state, action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    const data = await apiInstance.getEvents(action.payload.calendarId, {
      skip: action.payload.skip,
      limit: action.payload.limit
    })

    dispatch({ type: 'GET_EVENTS_SUCCESSFUL', payload: { events: deduplicate(state.events.concat(data), 'eventId') } })
  } catch (err) {
    dispatch({ type: 'GET_EVENTS_FAILED' })
  }
}

export const editEvent: SagaDispatcher<State, EditEvent, Actions> = async ({ action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    await apiInstance.editEvent(action.payload.eventId, {
      name: action.payload.name,
      description: action.payload.description,
      visibility: action.payload.visibility
    })

    dispatch({ type: 'EDIT_EVENT_SUCCESSFUL' })
  } catch (err) {
    dispatch({ type: 'EDIT_EVENT_FAILED' })
  }
}

export const createEvent: SagaDispatcher<State, CreateEvent, Actions> = async ({ action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    await apiInstance.createEvent(action.payload)

    dispatch({ type: 'CREATE_EVENT_SUCCESSFUL' })
  } catch (err) {
    dispatch({ type: 'CREATE_EVENT_FAILED' })
  }
}
