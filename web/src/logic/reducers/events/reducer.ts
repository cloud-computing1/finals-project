import { Reducer } from 'preact/hooks'
import { SagaDispatcher } from '../../types'
import { Actions } from '../main/types'
import { createEvent, editEvent, getEvents } from './sagas'
import { CreateEvent, EditEvent, GetEvents, State } from './types'

export const initialState: State = {
  loading: false,
  error: false,
  events: []
}

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'RESET_EVENTS':
      return {
        ...state,
        events: []
      }
    case 'GET_EVENTS':
    case 'EDIT_EVENT':
    case 'CREATE_EVENT':
      return {
        ...state,
        error: false,
        loading: true
      }
    case 'GET_EVENTS_FAILED':
    case 'EDIT_EVENT_FAILED':
    case 'CREATE_EVENT_FAILED':
      return {
        ...state,
        loading: false,
        error: true
      }
    case 'GET_EVENTS_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false,
        events: action.payload.events
      }
    case 'EDIT_EVENT_SUCCESSFUL':
    case 'CREATE_EVENT_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false
      }
    default:
      return state
  }
}

export const saga: SagaDispatcher<State, Actions, Actions> = ({ state, action, dispatch }) => {
  switch (action.type) {
    case 'GET_EVENTS':
      return getEvents({ state, action: action as GetEvents, dispatch })
    case 'EDIT_EVENT':
      return editEvent({ state, action: action as EditEvent, dispatch })
    case 'CREATE_EVENT':
      return createEvent({ state, action: action as CreateEvent, dispatch })
    default:
      return null
  }
}
