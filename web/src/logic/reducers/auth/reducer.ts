import { Reducer } from 'preact/hooks'
import { AfterPopulateAction } from '../../../hooks'
import { SagaDispatcher } from '../../types'
import { Actions } from '../main/types'
import { afterPopulate, login, register } from './sagas'
import { Login, Register, State } from './types'

export const initialState: State = {
  loading: false,
  error: false,
  isLoggedIn: false,
  token: '',
  userId: ''
}

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'REGISTER':
      return {
        ...state,
        error: false,
        loading: true
      }
    case 'REGISTER_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false
      }
    case 'REGISTER_FAILED':
      return {
        ...state,
        loading: false,
        error: true
      }
    case 'LOGOUT':
      return initialState
    case 'LOGIN':
      return {
        ...state,
        error: false,
        loading: true
      }
    case 'LOGIN_SUCCESSFUL':
      return {
        ...state,
        loading: false,
        error: false,
        isLoggedIn: true,
        userId: action.payload.userId,
        token: action.payload.token
      }
    case 'LOGIN_FAILED':
      return {
        ...state,
        loading: false,
        error: true,
        isLoggedIn: false
      }
    case 'POPULATE':
      return (action.payload as State) || initialState
    default:
      return state
  }
}

export const saga: SagaDispatcher<State, Actions, Actions> = ({ state, action, dispatch }) => {
  switch (action.type) {
    case 'LOGIN':
      return login({ state, action: action as Login, dispatch })
    case 'REGISTER':
      return register({ state, action: action as Register, dispatch })
    case 'AFTER_POPULATE':
      return afterPopulate({ state, action: action as AfterPopulateAction, dispatch })
    default:
      return null
  }
}
