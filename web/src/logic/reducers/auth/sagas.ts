import { api } from '../../../apis'
import { AfterPopulateAction } from '../../../hooks'
import { Logout } from '../../../typings'
import { SagaDispatcher } from '../../types'
import { Actions, Login, Register, State } from './types'

export const login: SagaDispatcher<State, Login, Actions> = async ({ action, dispatch }) => {
  const apiInstance = api.getInstance()
  // const firebaseInstance = firebase.getInstance()

  try {
    const data = await apiInstance.login(action.payload)
    // const subscriberToken = await firebaseInstance.getToken()

    apiInstance.authorize(data.session)

    // await apiInstance.subscribe(subscriberToken)

    dispatch({ type: 'LOGIN_SUCCESSFUL', payload: { token: data.session, userId: data.userId } })
  } catch (err) {
    console.error(err)
    dispatch({ type: 'LOGIN_FAILED' })
  }
}

export const logout: SagaDispatcher<State, Logout, Actions> = async () => {
  const apiInstance = api.getInstance()
  // const firebaseInstance = firebase.getInstance()

  try {
    // const subscriberToken = await firebaseInstance.getToken()
    // await apiInstance.unsubscribe(subscriberToken)
  } catch (err) {
    console.error('Error unsubscribing user', err.message, err.stack)
  }

  apiInstance.deAuthorize()
}

export const register: SagaDispatcher<State, Register, Actions> = async ({ action, dispatch }) => {
  const apiInstance = api.getInstance()

  try {
    await apiInstance.registerUser(action.payload)

    dispatch({ type: 'REGISTER_SUCCESSFUL' })
  } catch (err) {
    dispatch({ type: 'REGISTER_FAILED' })
  }
}

export const afterPopulate: SagaDispatcher<State, AfterPopulateAction, Actions> = async ({ state }) => {
  const apiInstance = api.getInstance()

  if (state.isLoggedIn && state.token !== '') {
    apiInstance.authorize(state.token as string)
  }
}
