import { AbstractAction } from '../../../typings'

export type Register = AbstractAction & {
  type: 'REGISTER';
  payload: { email: string; password: string; firstName: string; lastName: string };
};
export type RegisterFailed = AbstractAction & { type: 'REGISTER_FAILED' };
export type RegisterSuccessful = AbstractAction & { type: 'REGISTER_SUCCESSFUL' };

export type Logout = AbstractAction & { type: 'LOGOUT' };

export type Login = AbstractAction & { type: 'LOGIN'; payload: { email: string; password: string } };
export type LoginFailed = AbstractAction & { type: 'LOGIN_FAILED' };
export type LoginSuccessful = AbstractAction & { type: 'LOGIN_SUCCESSFUL'; payload: { userId: string; token: string } };

export type Actions = Register | RegisterFailed | RegisterSuccessful | Logout | Login | LoginFailed | LoginSuccessful;

export type State = {
  loading: boolean;
  error: boolean;
  isLoggedIn: boolean;
  userId: string;
  token: string;
};
