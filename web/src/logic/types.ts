export type ReducerWithSaga<State, Actions> = {
  state: State;
  dispatch: (action: Actions) => void;
};

export type SagaDispatcher<State, Actions, AllActions> = (saga: {
  state: State;
  action: Actions;
  dispatch: (action: AllActions) => void;
}) => void;
