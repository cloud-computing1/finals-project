import { createContext, FunctionalComponent, h } from 'preact'
import { useReducerWithSaga, useSaveReducer } from '../hooks'
import { main } from './reducers'
import { Actions, initialState, mainReducer, mainSagaDispatcher, State } from './reducers/main'
import { ReducerWithSaga } from './types'

export const Context = createContext<ReducerWithSaga<State, Actions>>({
  state: main.initialState,
  dispatch: () => {}
})

export const Store: FunctionalComponent = ({ children }) => {
  const [state, dispatch] = useReducerWithSaga(mainReducer, mainSagaDispatcher, initialState)

  useSaveReducer(state, dispatch, initialState)

  return (
    <Context.Provider value={{ state, dispatch }}>
      {children}
    </Context.Provider>
  )
}
