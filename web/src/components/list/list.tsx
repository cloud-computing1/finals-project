import { Fragment, FunctionalComponent, h } from "preact";
import { sizes } from "../../config";

interface Props {
  component?: FunctionalComponent
}

export const List: FunctionalComponent<Props> = ({ component: Component, children }) => {
  const UsedComponent = Component || Wrapper

  return (
    <Fragment>
      {Array.isArray(children)
        ? children.map(child => <UsedComponent>{child}</UsedComponent>)
        : <UsedComponent>{children}</UsedComponent>
      }
    </Fragment>
  )
}

const Wrapper: FunctionalComponent = ({ children }) => <div style={{ margin: sizes.l }}>{children}</div>
