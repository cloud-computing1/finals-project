import { Fragment, FunctionalComponent, h } from 'preact'
import { Link } from 'preact-router/match'
import { colors, sizes } from '../../config'
import { useIsLoggedIn } from '../../hooks'
import { Box } from '../box'

export const Header: FunctionalComponent = () => {
  const isLoggedIn = useIsLoggedIn()

  const linkStyle: h.JSX.CSSProperties = {
    color: colors.black,
    padding: sizes.xs,
    lineHeight: 1
  }

  const logoStyle: h.JSX.CSSProperties = {
    ...linkStyle,
    fontSize: '1.2rem',
    lineHeight: 0.8
  }

  return (
    <Box margin={'0'} padding={'0'} direction={'row'}>
      <Link href="/" style={logoStyle}>Calendary</Link>
      {!isLoggedIn
        ? (<Link href="/login" style={linkStyle}>🥷🏻 Login</Link>)
        : (
          <Fragment>
            <Link href="/calendars" style={linkStyle}>📅 Calendars</Link>
          </Fragment>
          )}

      {isLoggedIn && (
        <Fragment>
          <Box flex={1} padding={'0'} margin={'0'} noShadow />
          <Link href="/logout" style={linkStyle}>Logout 🚪🚶</Link>
        </Fragment>
      )}
    </Box>
  )
}
