import { FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { sizes } from '../../config'
import { PublicEvent } from '../../typings'
import { Box } from '../box'
import { Caption } from '../caption'

interface Props extends PublicEvent {
}

export const Event: FunctionalComponent<Props> = ({ eventId, ownerCalendarId, name, description, startDate, endDate }) => {
  const buttonStyle: h.JSX.CSSProperties = {
    margin: `0 ${sizes.xxs} 0 0`
  }

  const goBack = () => route('/calendars')
  const goToEditEvent = () => route(`/calendars/${ownerCalendarId}/events/${eventId}/edit`)
  const goToEventRSVPs = () => route(`/calendars/${ownerCalendarId}/events/${eventId}/rsvps`)

  return (
    <Box>
      <h1>⏱ {name}</h1>
      <Caption>Starts at {new Date(startDate).toLocaleDateString()}</Caption>
      <Caption>Ends at {new Date(endDate).toLocaleDateString()}</Caption>
      <p>{description}</p>
      <Box margin="0" padding="0" direction="row" justifyContent="flex-start" noShadow>
        <button onClick={goToEventRSVPs} style={buttonStyle}>View RSVPs</button>
        <button onClick={goBack} style={buttonStyle}>Go back</button>
        <button onClick={goToEditEvent}>Edit</button>
      </Box>
    </Box>
  )
}
