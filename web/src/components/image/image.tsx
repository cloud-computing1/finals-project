import { FunctionalComponent, h } from 'preact'
import { icons, images, svgs } from '../../assets'

interface Props {
  asset: keyof typeof icons | keyof typeof images | keyof typeof svgs
  height?: string,
  width?: string
}

export const Image: FunctionalComponent<Props> = ({ asset, height, width }) => {
  const src = (
    (asset in icons && icons[asset as keyof typeof icons]) ||
    (asset in images && images[asset as keyof typeof images]) ||
    (asset in svgs && svgs[asset as keyof typeof svgs]) ||
    svgs.exclamation
  )

  const style = {
    height: height || '2rem',
    width: width || '2rem'
  }

  return <img src={src} style={style} />
}
