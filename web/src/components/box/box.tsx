import { FunctionalComponent, h } from 'preact'
import { FlexStyle, flexStyle } from '../../utils'

interface Props extends FlexStyle {
}

export const Box: FunctionalComponent<Props> = ({ children, ...props }) => {
  const style = flexStyle(props)

  return (
    <div style={style}>
      {children}
    </div>
  )
}
