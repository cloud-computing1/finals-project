import { FunctionalComponent, h } from 'preact'
import { route } from 'preact-router'
import { sizes } from '../../config'
import { PublicCalendar } from '../../typings'
import { Box } from '../box'
import { Caption } from '../caption'

interface Props extends PublicCalendar {
}

export const Calendar: FunctionalComponent<Props> = ({ calendarId, name, description, visibility }) => {
  const buttonStyle: h.JSX.CSSProperties = {
    margin: `0 ${sizes.xxs} 0 0`
  }

  const goToViewCalendarFollowers = () => route(`/calendars/${calendarId}/followers`)
  const goToViewCalendarEvents = () => route(`/calendars/${calendarId}/events`)
  const goToEditCalendar = () => route(`/calendars/${calendarId}/edit`)

  return (
    <Box>
      <h1>📆 {name}</h1>
      <Caption>{visibility.substr(0, 1).toUpperCase()}{visibility.slice(1)} calendar</Caption>
      <p>{description}</p>
      <Box margin="0" padding="0" direction="row" justifyContent="flex-start" noShadow>
        <button onClick={goToViewCalendarFollowers} style={buttonStyle}>View followers</button>
        <button onClick={goToViewCalendarEvents} style={buttonStyle}>View events</button>
        <button onClick={goToEditCalendar}>Edit</button>
      </Box>
    </Box>
  )
}
