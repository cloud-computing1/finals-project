import { h, FunctionalComponent } from "preact";

interface Props {
}

export const Caption: FunctionalComponent<Props> = ({ children }) => {
  const style: h.JSX.CSSProperties = {
    textAlign: "left"
  }

  return (
    <caption style={style}>{children}</caption>
  )
}