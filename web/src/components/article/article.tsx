import { FunctionalComponent, h } from 'preact'
import { Box } from '../box'
import { Caption } from '../caption'

interface Props {
  title: string
  caption: string
  content: string
}

export const Article: FunctionalComponent<Props> = ({ title, caption, content }) => {
  return (
    <Box maxWidth={'75ch'}>
      <h1>{title}</h1>
      <Caption>{caption}</Caption>
      {content.split('\n').map((paragraph, index) => <p key={`${paragraph.substr(0, 16)}${index}`}>{paragraph}</p>)}
    </Box>
  )
}
