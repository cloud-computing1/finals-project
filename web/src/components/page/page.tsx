import { ComponentChild, FunctionalComponent, h } from 'preact'
import { Box } from '../box'

interface Props {
  justifyContent?: 'flex-start' | 'center' | 'flex-end' | 'stretch'
  alignItems?: 'flex-start' | 'center' | 'flex-end' | 'stretch'

  padding?: string

  children?: ComponentChild
}

export const Page: FunctionalComponent<Props> = ({ justifyContent, alignItems, padding, children }) => {
  return (
    <Box margin="0" padding={padding || '0'} flex={1} overflow="scroll" justifyContent={justifyContent} alignItems={alignItems} noShadow>
      {children}
    </Box>
  )
}
