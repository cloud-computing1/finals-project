import { FunctionalComponent, h } from "preact";
import { flexStyle } from "../../utils";

interface Props {
  size: string
}

export const Spaced: FunctionalComponent<Props> = ({ size, children }) => {
  const style = flexStyle({
    flex: 1,
    justifyContent: "stretch",
    alignItems: "stretch",
    margin: "0",
    noShadow: true,
    padding: size
  })

  return (
    <div style={style}>
      {children}
    </div>
  )
}

