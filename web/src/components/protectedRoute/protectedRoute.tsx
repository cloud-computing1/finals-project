import { h, VNode } from 'preact'
import { Route, route, RouteProps } from 'preact-router'
import { useIsLoggedIn } from '../../hooks'

export function ProtectedRoute <Props> (props: RouteProps<Props> & Partial<Props>): VNode {
  const isLoggedIn = useIsLoggedIn()

  if (!isLoggedIn) {
    route('/login', true)
  }

  return <Route {...props} />
}
