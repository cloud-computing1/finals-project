import { FunctionalComponent, h } from 'preact'

interface Props {
  animation: string
}

export const Animated: FunctionalComponent<Props> = ({ animation, children }) => {
  const style: h.JSX.CSSProperties = {
    lineHeight: 0
  }

  return (
    <span style={style} className={animation}>{children}</span>
  )
}
