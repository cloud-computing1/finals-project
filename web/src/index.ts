import { App } from './app'
import './style/style.css'
import './style/animations.css'

export default App
