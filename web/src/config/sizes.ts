export const sizes = {
  xxxs: '4px',
  xxs: '8px',
  xs: '16px',
  s: '24px',
  m: '36px',
  l: '48px',
  xl: '64px',
  xxl: '128px',
  xxxl: '256px',
}