export const colors = {
  white: "hsl(0, 0%, 100%)",
  black: 'hsl(0, 0%, 10%)',

  neutral1: 'hsl(0, 0%, 90%)',
  neutral2: 'hsl(0, 0%, 80%)',
  neutral3: 'hsl(0, 0%, 70%)',
  neutral4: 'hsl(0, 0%, 60%)',
  neutral5: 'hsl(0, 0%, 50%)',
  
}